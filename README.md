    CREATE TABLE Cinema
    (
    cinema_id SERIAL PRIMARY KEY,
    cinema_name varchar(30) NOT NULL,
    cinema_capacity int NOT NULL
    );
    
    CREATE TABLE Screen
    (
    screen_id SERIAL PRIMARY KEY,
    screen_model varchar(30) NOT NULL,
    screen_3D bool NOT NULL,
    cinema_id int not null,
    CONSTRAINT fk_cinema FOREIGN KEY (cinema_id) REFERENCES Cinema(cinema_id)
    );
    
    select * from Screen;
    select * from Cinema;
    
    INSERT INTO Cinema(cinema_name, cinema_capacity)
    VALUES ('Multiplex', 1500);
    
    INSERT INTO Cinema(cinema_name, cinema_capacity)
    VALUES ('Olimp', 560);
    
    INSERT INTO Cinema(cinema_name, cinema_capacity)
    VALUES ('Butterfly', 800);
    INSERT INTO Screen(screen_model, screen_3D, cinema_id)
    VALUES ('big', 'true' , 1);
    INSERT INTO Screen(screen_model, screen_3D, cinema_id)
    VALUES ('big', 'false' , 3);
    INSERT INTO Screen(screen_model, screen_3D, cinema_id)
    VALUES ('mid', 'true' , 2);
    INSERT INTO Screen(screen_model, screen_3D, cinema_id)
    VALUES ('small', 'false' , 2);
    INSERT INTO Screen(screen_model, screen_3D, cinema_id)
    VALUES ('tiny', 'true' , 3);
    


