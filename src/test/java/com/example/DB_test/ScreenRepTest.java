package com.example.DB_test;

import com.example.DB_test.model.Cinema;
import com.example.DB_test.model.Screen;
import com.example.DB_test.repository.ScreenRep;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ScreenRepTest {
    private final Screen screen = new Screen(1, "big", true, new Cinema(1, "test1", 1));
    private final Screen screen2 = new Screen(2, "big", false, new Cinema(1, "test1", 1));
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ScreenRep screenRep;

    @SneakyThrows
    @Test
    public void finaAllScreenTest() {
        Mockito.when(screenRep.findAll()).thenReturn(Arrays.asList(screen, screen2));
        mockMvc.perform(get("/screen/findAll"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Arrays.asList(screen, screen2))));
    }

    @SneakyThrows
    @Test
    public void findScreenByIdTest() {
        Mockito.when(screenRep.findById(Mockito.any())).thenReturn(Optional.of(screen));

        mockMvc.perform(get("/screen/findById/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.model").value("big"));
    }

    @SneakyThrows
    @Test
    public void addNewScreenTest() {
        Mockito.when(screenRep.save(Mockito.any())).thenReturn(screen);

        mockMvc.perform(post("/screen/createScreen").content(objectMapper.writeValueAsString(screen))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.model").value("big"));
    }

    @SneakyThrows
    @Test
    public void updateScreenTest() {
        Mockito.when(screenRep.save(Mockito.any())).thenReturn(screen);
        Mockito.when(screenRep.findById(Mockito.any())).thenReturn(Optional.of(screen));

        mockMvc.perform(put("/screen/updateScreen/1")
                        .content(objectMapper
                                .writeValueAsString(new Screen(1, "big", true, new Cinema(1, "test1", 1))))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.model").value("big"));
    }

    @SneakyThrows
    @Test
    public void deleteScreenByIdTest() {
        Mockito.when(screenRep.findById(Mockito.any())).thenReturn(Optional.of(screen));

        mockMvc.perform(delete("/screen/delete/1")).andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    public void findByTwoParamTest() {
        Mockito.when(screenRep.findByTwoParams(Mockito.any(), Mockito.any())).thenReturn(List.of(screen));

        mockMvc.perform(get("/screen/param?model=big&flag=true"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(List.of(screen))));
    }
}

