package com.example.DB_test;

import com.example.DB_test.model.Cinema;
import com.example.DB_test.repository.CinemaRep;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CinemaRepTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CinemaRep cinemaRep;

    @SneakyThrows
    @Test
    public void getListOfCinemaTest() {
        Cinema c1 = new Cinema(1, "test1", 12);
        Cinema c2 = new Cinema(2, "test2", 21);
        Mockito.when(cinemaRep.findAll()).thenReturn(Arrays.asList(c1, c2));
        mockMvc.perform(get("/cinema/findAll"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Arrays.asList(c1, c2))));
    }
}



