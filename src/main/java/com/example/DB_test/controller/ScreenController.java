package com.example.DB_test.controller;

import com.example.DB_test.model.Screen;
import com.example.DB_test.service.DefaultScreenService;
import com.example.DB_test.service.ScreenPageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/screen")
@AllArgsConstructor
public class ScreenController {
    private final DefaultScreenService service;

    private final ScreenPageService screenPageService;

    @GetMapping()
    public ResponseEntity<List<Screen>> getAllScreen(@RequestParam(defaultValue = "0") Integer pageNo,
                                                     @RequestParam(defaultValue = "2") Integer pageSize,
                                                     @RequestParam(defaultValue = "id") String sortBy) {
        List<Screen> list = screenPageService.getAllScreen(pageNo, pageSize, sortBy);

        return new ResponseEntity<List<Screen>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/findAll")
    public List<Screen> findAllScreen() {
        return service.findAll();
    }

    @GetMapping("/findById/{id}")
    public Optional<Screen> findScreenById(@PathVariable Integer id) {
        return service.findById(id);
    }

    @PostMapping("/createScreen")
    public Screen addNewScreen(@RequestBody Screen screen) {
        return service.save(screen);
    }

    @PutMapping("/updateScreen/{id}")
    public ResponseEntity<Screen> updateScreen(@PathVariable Integer id, @RequestBody Screen screenDetails) {
        Screen updateScreen = service.findById(id)
                .orElseThrow(() -> new RuntimeException("Screen not exist with id: " + id));
        updateScreen.setModel(screenDetails.getModel());
        updateScreen.setScreen3D(screenDetails.getScreen3D());
        updateScreen.setCinema(screenDetails.getCinema());

        service.save(updateScreen);

        return ResponseEntity.ok(updateScreen);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteScreenById(@PathVariable Integer id) {
        service.delete(id);
    }

    @GetMapping("/param")
    public List<Screen> findByTwoParam(@RequestParam String model, @RequestParam Boolean flag) {
        return service.findByTwoParams(model, flag);
    }
}
