package com.example.DB_test.controller;

import com.example.DB_test.model.Cinema;
import com.example.DB_test.service.DefaultCinemaService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cinema")
@AllArgsConstructor
public class CinemaController {

    private final DefaultCinemaService service;

    @GetMapping("/findAll")
    public List<Cinema> findAllCinema() {
        return service.findAllCinema();
    }
}
