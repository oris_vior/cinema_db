package com.example.DB_test.service;

import com.example.DB_test.model.Screen;
import com.example.DB_test.repository.ScreenPageRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

@Service
public class ScreenPageService {
    @Autowired
    ScreenPageRep screenPageRep;

    public List<Screen> getAllScreen(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<Screen> pageResult = screenPageRep.findAll(paging);
        if (pageResult.hasContent()) {
            return pageResult.getContent();
        }

           return new ArrayList<Screen>();

    }
}
