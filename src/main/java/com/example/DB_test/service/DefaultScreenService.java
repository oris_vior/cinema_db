package com.example.DB_test.service;

import com.example.DB_test.model.Screen;
import com.example.DB_test.repository.ScreenRep;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DefaultScreenService implements ScreenService {

    private final ScreenRep screenRep;



    public List<Screen> findByTwoParams(String model, Boolean flag) {
        return screenRep.findByTwoParams(model, flag);
    }

    @Override

    public List<Screen> findAll() {
        return screenRep.findAll();
    }

    @Override
    public Optional<Screen> findById(Integer id) {
        return screenRep.findById(id);
    }

    @Override
    public Screen save(Screen screen) {
        return screenRep.save(screen);
    }


    @Override
    public void delete(Integer id) {
        screenRep.deleteById(id);
    }
}
