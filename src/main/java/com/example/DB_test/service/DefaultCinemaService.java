package com.example.DB_test.service;

import com.example.DB_test.model.Cinema;
import com.example.DB_test.repository.CinemaRep;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class DefaultCinemaService implements CinemaService {

   private final CinemaRep cinemaRep;

    @Override
    public List<Cinema> findAllCinema() {
        return cinemaRep.findAll();
    }
}
