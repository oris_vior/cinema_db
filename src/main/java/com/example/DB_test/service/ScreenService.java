package com.example.DB_test.service;

import com.example.DB_test.model.Screen;

import java.util.List;
import java.util.Optional;

public interface ScreenService {
    List<Screen> findAll();

    Optional<Screen> findById(Integer id);

    Screen save(Screen screen);



    void delete(Integer id);

}
