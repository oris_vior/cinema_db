package com.example.DB_test.service;

import com.example.DB_test.model.Cinema;

import java.util.List;

public interface CinemaService {
    List<Cinema> findAllCinema();
}
