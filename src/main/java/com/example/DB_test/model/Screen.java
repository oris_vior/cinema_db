package com.example.DB_test.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "screen")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Screen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "screen_id")
    private Integer id;
    @Column(name = "screen_model")
    private String model;
    @Column(name = "screen_3d")
    private Boolean screen3D;
    @ManyToOne
    @JoinColumn(name = "cinema_id")
    private Cinema cinema;
}
