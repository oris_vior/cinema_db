package com.example.DB_test.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "Cinema")
public class Cinema {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cinema_id")
    private Integer id;
    @Column(name = "cinema_name")
    private String name;
    @Column(name = "cinema_capacity")
    private Integer capacity;

    public Cinema(String name) {
        this.name = name;
    }
}
