package com.example.DB_test.repository;

import com.example.DB_test.model.Screen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ScreenRep extends JpaRepository<Screen, Integer> {
    @Override
    List<Screen> findAll();

    @Override
    Optional<Screen> findById(Integer id);

    @Override
    <S extends Screen> S save(S entity);

    @Query("select t from Screen t where t.model = ?1 and t.screen3D = ?2")
    List<Screen> findByTwoParams(String model, Boolean flag);

    @Override
    void deleteById(Integer id);
}
