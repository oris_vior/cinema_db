package com.example.DB_test.repository;

import com.example.DB_test.model.Cinema;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CinemaRep extends JpaRepository<Cinema, Integer> {
    @Override
    List<Cinema> findAll();
}
