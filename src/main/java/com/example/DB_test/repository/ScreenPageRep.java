package com.example.DB_test.repository;

import com.example.DB_test.model.Screen;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScreenPageRep extends PagingAndSortingRepository<Screen, Integer> {
    @Override
    Page<Screen> findAll(Pageable pageable);
}
